VH_FileLib = { }

VH_FileLib.Side = {

	Client = 0,

	Server = 1,

}

function VH_FileLib:VH_Include( Location )

	table.insert( self.Loaded, Location )

	include( Location )

end

function VH_FileLib:Initialise( Location )

	self.Loaded = { }

	table.insert( self.Loaded, Location .. "/vh-filelib.lua" )

	self.Initialised = true

	return self

end

function VH_FileLib:IncludeFile( Location, Side )

	if not self.Initialised then

		print( "VH_FileLib not initialised! Please call VH_FileLib.initialise( FileLibLocation ) before using VH_FileLib" )

	end

	if table.HasValue( self.Loaded, Location ) then

		return

	end

	if Side == self.Side.Client then

		if SERVER then

			AddCSLuaFile( Location )

		else

			VH_FileLib:VH_Include( Location )

		end

	elseif Side == self.Side.Server then

		if SERVER then

			VH_FileLib:VH_Include( Location )

		end

	else

		if SERVER then

			AddCSLuaFile( Location )

		end

		VH_FileLib:VH_Include( Location )

	end

	return self

end

function VH_FileLib:IncludeDir( Dir )

	local Files, Dirs = file.Find( Dir .. "/*", "LUA" )

	for a, b in ipairs( Files ) do

		if string.lower( string.Right( b, 4 ) ) == ".lua" then

			local prefix = string.lower( string.Left( b, 3 ) )

			self:IncludeFile( Dir .. "/" .. b,  prefix == "cl_" and self.Side.Client or ( prefix == "sv_" and self.Side.Server or ( nil ) ) )

		end

	end

	for a, b in ipairs( Dirs ) do

		self:IncludeDir( Dir .. "/" .. b )

	end

	return self

end

function VH_FileLib:AddResourceDir( Dir )

	local Files, Dirs = file.Find( Dir .. "/*", "LUA" )

	for a, b in ipairs( Files ) do

		resource.AddFile( Dir .. "/" .. b )

	end

	for a, b in ipairs( Dirs ) do

		self:AddResourceDir( Dir .. "/" .. b )

	end

	return self

end
